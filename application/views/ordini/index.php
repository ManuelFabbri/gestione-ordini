<h2><?php echo $title ?></h2>

<?php foreach ($ordini as $ordine) : ?>
<div class="ordine">

<h4>Ordine n. <?php echo $ordine['id']; ?></h4>
<hr>
<div class="row">
    <div class="col-sm-4">
        Nome: <b> <?php echo $ordine['nome'] ?> </b><br/>
        Cognome:<b> <?php echo $ordine['cognome'] ?> </b><br/>
        Città:<b> <?php echo $ordine['citta'] ?> </b><br/>
        Indirizzo: <?php echo '<i>Via/Viale/P.zza</i> <b>'.$ordine['via']. ' ' .$ordine['numero_civico'] ?> </b><br/>
    </div>
    <div class="col-sm-4">
        Telefono: <b> <?php echo $ordine['telefono'] ?> </b><br/>
        E-mail: <b> <?php echo $ordine['email'] ?> </b><br/>
        Orario Ordine: <b> <?php echo $this->ordini_model->mysqlToUser($ordine['orario']); ?> </b><br/>
        Importo: <b> <?php echo $ordine['importo']."€" ?> </b>
    </div>
    <div class="col-sm-4">
        Stato ordine: <br>
        <?php echo "<b>".ucfirst($ordine['stato_ordine'])."</b>"; ?> <br> <br>
        <a href="<?php echo site_url("ordini/edit/".$ordine['id']); ?>" class="btn btn-warning" title="Modifica"><i class="fas fa-edit"></i></a>
    </div>
    
</div>


</div>

<?php endforeach; ?>