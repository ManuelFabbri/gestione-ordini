<h2><?php echo $title; ?></h2>


<?php echo form_open('ordini/edit/'.$id); ?>
<div class="form-group">
    <select class="form-control" id="stato_ordine">
        <option value="elaborazione" <?php echo $isSelected = ($stato_ordine=='elaborazione') ? 'selected' : '' ?>>In Elaborazione</option>
        <option value="imballaggio" <?php echo $isSelected = ($stato_ordine=='imballaggio') ? 'selected' : '' ?>>Imballaggio</option>
        <option value="evaso" <?php echo $isSelected = ($stato_ordine=='evaso') ? 'selected' : '' ?>>Evaso</option>
        <option value="consegnato" <?php echo $isSelected = ($stato_ordine=='consegnato') ? 'selected' : '' ?>>Consegnato</option>
        <option value="restituito" <?php echo $isSelected = ($stato_ordine=='restituito') ? 'selected' : '' ?>>Restituito</option>
    </select>
    <br>
</div>
<button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Salva</button>
<a href="<?php echo site_url('ordini/index'); ?>"><button type="button" class="btn btn-danger"><i class="fas fa-times"></i> Annulla</button></a>
<?php echo form_close(); ?>

    
