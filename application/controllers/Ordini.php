<?php 
    class Ordini extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('ordini_model');
            $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title']="Gestione Ordini";
        $data['ordini']= $this->ordini_model->getOrdini();

        $this->load->view('commons/header', $data);
        $this->load->view('ordini/index', $data);
        $this->load->view('commons/footer');
    }

    public function edit($id) {
            $stato_ordine = $this->ordini_model->getOrdine($id);

        //  echo "<pre>";
        //  print_r($results);
        //  die();
    
            $data['title'] = 'Modifica Ordine';
            $data['stato_ordine'] = $stato_ordine[0]['stato_ordine'];
            $data['id'] = $id;
    
            $this->load->helper('form');
            $this->load->library('form_validation');
    
            $this->form_validation->set_rules('stato_ordine','Stato ordine corrente','required');
             
            if($this->form_validation->run() === FALSE) {
                //Validazione fallita o form non inviato
                $this->load->view('commons/header',$data);
                $this->load->view('ordini/edit', $data);
                $this->load->view('commons/footer');
                echo "non dentro";
        } else {
                //Validazione ok
                echo "dentro";
                    $array_ass_dati = array(
                        'stato_ordine' => $this->input->post('stato_ordine'), //$_POST['nome']
                    );
    
                    $this->ordini_model->updateStatusOrdini($id, $array_ass_dati);
                
                redirect('ordini/index');    
        }
    }
}
