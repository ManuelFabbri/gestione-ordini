<?php
class Prodotti_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function getProduct(){
            $query = $this->db->get('prodotti');
            return $query->result_array();
        }

        public function getSingleProduct($id) {
            return $this->db->get_where("prodotti", array("id" => $id));
        }

        public function get_prodotto_row($id){
            $query = $this->db->get_where('prodotti', array('id' => $id));
            return $query->row_array();
        }

        public function update_prodotto($id, $data){
            $this->db->where('id', $id);
            $this->db->update('prodotti', $data);
        }

        public function create_prodotto($data) {
            $this->db->insert('prodotti', $data);
        }

        
}