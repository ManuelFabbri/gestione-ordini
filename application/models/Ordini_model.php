<?php
class Ordini_model extends CI_Model {

    public function __construct()
    {
            $this->load->database();
    }

    public function getOrdini(){
        $query = $this->db->get('ordini');
        return $query->result_array();
    }

    public function getOrdine($id){
        $this->db->select('stato_ordine');
        $query = $this->db->get_where('ordini', array('id' => $id));
        return $query->result_array();
    }

    public function mysqlToUser($date){
        $datasplit = explode(" ", $date);
        
        $singledata = explode('-', $datasplit[0]);

        return $dataeurope = $singledata[2]. "-" .$singledata[1]. "-" . $singledata[0] . " " . $datasplit[1];
    }

    public function getStatus() {
        $this->db->select("stato_ordine");
        return $query = $this->db->get('ordini');
    }

    public function updateStatusOrdini($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('ordini', $data);
    }
}